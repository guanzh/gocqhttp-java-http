package com.plugin.gocqhttp.api;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.plugin.gocqhttp.configuration.GoCqhttpPropertis;
import com.plugin.gocqhttp.pojo.GroupMemberInfo;
import com.plugin.gocqhttp.pojo.ResponseInfo;
import com.plugin.gocqhttp.pojo.ResponseList;
import com.plugin.gocqhttp.pojo.api.SetFriendAddRequest;
import com.plugin.gocqhttp.pojo.event.EventGroupAdd;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 下线接口  请求服务端
 */
@Component
@Slf4j
public class GocqApiHttpService {
    private GoCqhttpPropertis goCqhttpPropertis;
    public GocqApiHttpService(GoCqhttpPropertis goCqhttpPropertis){
        this.goCqhttpPropertis = goCqhttpPropertis;
    }

// ============================================================================================================
// ============================================================================================================
// ============================================================================================================
    public String getRequestUrl(String mainUrl){
        if (mainUrl.endsWith("/")){
            return mainUrl ;
        }
      return   mainUrl + "/";
    }

    /**
     *
     * @param param
     * @return
     */
    public String request(Object param) {
        Class<?> requestClazz = param.getClass();
        GocqApiConstans.RequestApi existsApi = Arrays.stream(GocqApiConstans.RequestApi.values()).filter(item -> item.getClazz().equals(requestClazz)).collect(Collectors.toList()).stream().findFirst().get();
        if (null == existsApi){throw  new RuntimeException("not has GocqApiConstans.RequestApi clazz");};
        StringBuffer requestUrl = new StringBuffer();
        requestUrl.append(getRequestUrl(goCqhttpPropertis.getAddr()));
        requestUrl.append(existsApi.getApiAdr());
        Map map = JSON.toJavaObject(JSONObject.parseObject(JSON.toJSONString(param)), Map.class);
        String response = HttpUtil.get(requestUrl.toString(), map);
        log.info("url:{} param:{} ==>发送成功:{} ", requestUrl.toString(), JSON.toJSONString(map),response );
        return response;
    }
}
