package com.plugin.gocqhttp.event;

import com.alibaba.fastjson.JSONObject;

public  interface EventTypeAdaptHandler<T> extends EventTypeHandler {

     Object work(JSONObject requestParam, T t);


}
