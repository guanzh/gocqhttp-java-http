package com.plugin.gocqhttp.event;

import com.alibaba.fastjson.JSONObject;

/**
 * 时间类型 策略模式接口
 */
public interface EventTypeHandler{

     boolean suppert(JSONObject requestParam);

     Object handlerRequest(JSONObject requestParam);
}
