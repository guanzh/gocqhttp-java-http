package com.plugin.gocqhttp.event.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.plugin.gocqhttp.event.AbstractEventTypeSupperGenericsSimple;
import com.plugin.gocqhttp.pojo.event.EventPrivate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 适配后实现自己的方法
 * 收到私聊消息
 */

@Slf4j
public class EventPrivateHandlerImpl extends AbstractEventTypeSupperGenericsSimple<EventPrivate> {

    @Override
    public Object work(JSONObject requestParam, EventPrivate event) {
        log.info("私聊消息{}", JSON.toJSONString(requestParam));
        // todo 对接自己的机器人；回复等等
          return null;
    }

}
