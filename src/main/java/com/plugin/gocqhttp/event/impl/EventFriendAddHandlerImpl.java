package com.plugin.gocqhttp.event.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.plugin.gocqhttp.api.GocqApiHttpService;
import com.plugin.gocqhttp.event.AbstractEventTypeSupperGenericsSimple;
import com.plugin.gocqhttp.pojo.api.SetFriendAddRequest;
import com.plugin.gocqhttp.pojo.event.EventFriendAdd;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 适配后实现自己的方法
 * 好友添加，自动通过
 * @author zehua
 */
@Slf4j
public class EventFriendAddHandlerImpl extends AbstractEventTypeSupperGenericsSimple<EventFriendAdd> {

    @Autowired
    GocqApiHttpService gocqApiHttpService;

    @Override
    public Object work(JSONObject requestParam, EventFriendAdd event) {
        log.info("私聊消息{}", JSON.toJSONString(requestParam));
        //统一好友请求
        SetFriendAddRequest setFriendAddRequest = new SetFriendAddRequest();
        setFriendAddRequest.setFlag(event.getFlag());
        setFriendAddRequest.setApprove(true);
        return gocqApiHttpService.request(setFriendAddRequest);
    }


}
