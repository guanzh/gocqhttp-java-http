package com.plugin.gocqhttp.event.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.plugin.gocqhttp.api.GocqApiHttpService;
import com.plugin.gocqhttp.event.AbstractEventTypeSupperGenericsExt;
import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.api.SetGroupAddRequest;
import com.plugin.gocqhttp.pojo.event.EventGroupAdd;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 适配后实现自己的方法
 * @title 收到入群邀请通知
 * @author zehua
 * @author zehua
 */

@Slf4j
public class EventGroupAddHandlerImpl extends AbstractEventTypeSupperGenericsExt<EventGroupAdd> {

    @Autowired
    GocqApiHttpService gocqApiHttpService;

    @Override
    public Object work(JSONObject requestParam, EventGroupAdd event) {
        log.info("入群邀请{}", JSON.toJSONString(requestParam));
        // 加群自动同意
        SetGroupAddRequest setGroupAddRequest = new SetGroupAddRequest();
        setGroupAddRequest.setApprove(true);
        setGroupAddRequest.setFlag(event.getFlag());
        return gocqApiHttpService.request(event);
    }

    @Override
    public boolean checkArgs(EventGroupAdd event) {
        if (null != event.getRequest_type() && event.getRequest_type().equals(EventMessageTypeConstans.GROUP) && (event.getSub_type().equals(EventMessageTypeConstans.SUB_TYPE_ADD) || event.getSub_type().equals(EventMessageTypeConstans.SUB_TYPE_INVITE))){
            return true;
        }
        return false;
    }


}
