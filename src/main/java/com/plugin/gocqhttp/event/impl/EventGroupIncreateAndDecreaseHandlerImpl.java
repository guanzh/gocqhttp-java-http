package com.plugin.gocqhttp.event.impl;

import com.alibaba.fastjson.JSONObject;
import com.plugin.gocqhttp.event.AbstractEventTypeSupperGenericsExt;
import com.plugin.gocqhttp.api.GocqApiHttpService;
import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.api.SetGroupAddRequest;
import com.plugin.gocqhttp.pojo.event.EventGroupIncreaseAndDecrease;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 适配后实现自己的方法
 * 其它群人员新增和减少 通知加入自己的群
 */

public class EventGroupIncreateAndDecreaseHandlerImpl extends AbstractEventTypeSupperGenericsExt<EventGroupIncreaseAndDecrease> {

    @Autowired
    GocqApiHttpService gocqApiHttpService;

    @Override
    public Object work(JSONObject requestParam, EventGroupIncreaseAndDecrease event) {
        //todo 实现自己的方法
        return null;
    }

    @Override
    public boolean checkArgs(EventGroupIncreaseAndDecrease event) {
        if (null == event){return false;}
        if (null != event.getNotice_type() && (event.getNotice_type().equals(EventMessageTypeConstans.GROUP_INCREASE)
                || event.getNotice_type().equals(EventMessageTypeConstans.GROUP_DECREASE))) {
            return true;
        }
        return false;

    }


}
