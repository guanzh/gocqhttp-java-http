package com.plugin.gocqhttp.event.impl;

import com.alibaba.fastjson.JSONObject;
import com.plugin.gocqhttp.api.GocqApiHttpService;
import com.plugin.gocqhttp.event.AbstractEventTypeSupperGenericsSimple;
import com.plugin.gocqhttp.pojo.event.EventGroup;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 适配后实现自己的方法  消息存储
 * @author zehua
 */
//
//@Order(Integer.MAX_VALUE)
public class EventGroupStoreHandlerImpl extends AbstractEventTypeSupperGenericsSimple<EventGroup> {

    @Autowired
    GocqApiHttpService gocqApiHttpService;

    @Override
    public Object work(JSONObject requestParam, EventGroup eventGroup) {
        //todo 实现自己的方法
        return null;
    }

    //    @Override
//    public Class getEntityClass() {
//        return null;
//    }
}
