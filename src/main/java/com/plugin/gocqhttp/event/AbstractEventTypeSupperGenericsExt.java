package com.plugin.gocqhttp.event;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONObject;

/**
 * 通用接口 用户自定义扩展方法
 * @param <T>
 */
public  abstract  class AbstractEventTypeSupperGenericsExt<T> extends AbstractEventTypeSupperGenericsSimple<T> {

    @Override
    public boolean suppert(JSONObject requestParam) {
         if (!super.suppert(requestParam)){return false;}
         if (!checkArgs(super.object)){return false;}
        return true;
    }

    public abstract boolean checkArgs(T t);

}
