package com.plugin.gocqhttp.event;

import com.plugin.gocqhttp.api.GocqApiHttpService;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.ParameterizedType;

/**
 * 通用接口 用户自定义扩展方法
 * @param <T>
 */
public  abstract  class AbstractEventTypeSupperGenericsSimple<T> extends AbstractEventTypeSupperGenerics<T> {

    @Autowired
    protected GocqApiHttpService gocqhttpService;

    @Override
    public Class getEntityClass() {
        ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
        return (Class<T>) parameterizedType.getActualTypeArguments()[0];
    }



}
