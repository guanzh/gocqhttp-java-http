package com.plugin.gocqhttp.event;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSON;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;

import java.lang.reflect.Field;

@Deprecated
public abstract class AbstractEventTypeSuppertDelege<T> implements EventTypeHandler {

    T event;

    public AbstractEventTypeSuppertDelege(T event) {
        this.event = event;
    }

    @Override
    public boolean suppert(JSONObject requestParam) {
        T parseObj = getParseObj(requestParam);
        if (null == parseObj) {
            return false;
        }
        this.event = parseObj;
        if (!checkRequiredAnnotion()) return true;
        return true;
    }

    /**
     * ADD ReuireValue check
     * @return
     */
    private boolean checkRequiredAnnotion() {
        Field[] fields = this.event.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            RequireValue annotation = field.getAnnotation(RequireValue.class);
            try {
                if (null != annotation && !StrUtil.isEmptyIfStr(annotation.value()) && null != field.get(this.event)){
                   if (!annotation.value().equals(field.get(this.event))){
                       return false;
                   }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return true;
    }



    public T getParseObj(JSONObject requestParam){
        return (T) JSON.toJavaObject(requestParam, this.event.getClass());
    }





}
