package com.plugin.gocqhttp.pojo;

import lombok.Data;

/**
 * 群成员信息
 */
@Data
public class GroupMemberInfo {
    private int age;
    private String area;
    private String card;
    private boolean card_changeable;
    private int group_id;
    private int join_time;
    private int last_sent_time;
    private String level;
    private String nickname;
    private String role;
    private String sex;
    private int shut_up_timestamp;
    private String title;
    private int title_expire_time;
    private boolean unfriendly;
    private int user_id;
}
