package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 事件数据

 字段名	数据类型	可能的值	说明
 post_type	string	notice	上报类型
 notice_type	string	notify	消息类型
 sub_type	string	poke	提示类型
 self_id	int64		BOT QQ 号
 sender_id	int64		发送者 QQ 号
 user_id	int64		发送者 QQ 号
 target_id	int64		被戳者 QQ 号
 time	int64		时间
 #群内戳一戳
 */
@Data
public class EventFriendPoke {
    @RequireValue(EventMessageTypeConstans.NOTICE)
    private String post_type;
    private String notice_type;
    @RequireValue(EventMessageTypeConstans.POKE)
    private String sub_type;
    private int self_id	;
    private  long sender_id;
    private   long   user_id;
    private  long target_id;
    private  long time;

}
