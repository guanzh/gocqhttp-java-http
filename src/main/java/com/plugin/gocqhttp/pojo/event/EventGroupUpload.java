package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

@Data
public class EventGroupUpload {
    private int time;
    private int self_id;

    @RequireValue(value = EventMessageTypeConstans.NOTICE)
    private String post_type;
    @RequireValue(value = EventMessageTypeConstans.GROUP_UPLOAD)
    private String notice_type;
    private int group_id;
    private int user_id;
    private UploadDTO file;

    public static class UploadDTO {
        private String id;
        private String naem;
        private int size;
        private int busid;
    }



}
