package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 好友消息撤回
 事件数据

 字段名	数据类型	可能的值	说明
 time	int64	-	事件发生的时间戳
 self_id	int64	-	收到事件的机器人 QQ 号
 post_type	string	notice	上报类型
 notice_type	string	friend_recall	通知类型
 user_id	int64		好友 QQ 号
 message_id	int64		被撤回的消息 ID
 */
@Data
public class EventFriendRecall {

    private  long time;
    private int self_id	;
    @RequireValue(value = EventMessageTypeConstans.NOTICE)
    private String post_type;
    @RequireValue(value = EventMessageTypeConstans.FRIEND_RECALL)
    private String notice_type;
    private   long   user_id;
    private  long message_id;

}
