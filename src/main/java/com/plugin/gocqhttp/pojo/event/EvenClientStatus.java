package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 * 其他客户端在线状态变更
事件数据

字段	类型	可能的值	说明
post_type	string	notice	上报类型
notice_type	string	client_status	消息类型
client	Device*		客户端信息
online	bool		当前是否在线
 */
@Data
public class EvenClientStatus {
    @RequireValue(EventMessageTypeConstans.NOTICE)
    private String post_type;
    @RequireValue(EventMessageTypeConstans.CLIENT_STATUS)
    private String notice_type;
    private Device client;
    private Boolean online;

    /**
     * app_id	int64	客户端ID
     * device_name	string	设备名称
     * device_kind	string	设备类型
     */
    class Device{
        private  long app_id;
        private  String device_name;
        private  String device_kind;
    }
}
