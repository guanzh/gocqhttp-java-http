package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 * 精华消息
事件数据

字段	类型	可能的值	说明
post_type	string	notice	上报类型
notice_type	string	essence	消息类型
sub_type	string	add,delete	添加为add,移出为delete
sender_id	int64		消息发送者ID
operator_id	int64		操作者ID
message_id	int32		消息ID
 */
@Data
public class EvenEssence {
    private String post_type;
    @RequireValue(EventMessageTypeConstans.ESSENCE)
    private String notice_type;
    private String sub_type;
    private int sender_id;
    private int operator_id;
    private int message_id;
}
   
