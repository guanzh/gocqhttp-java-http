package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 群红包运气王提示
 注意

 此事件无法在手表协议上触发

 事件数据

 字段	类型	可能的值	说明
 post_type	string	notice	上报类型
 notice_type	string	notify	消息类型
 group_id	int64		群号
 sub_type	string	lucky_king	提示类型
 user_id	int64		红包发送者id
 target_id	int64		运气王id
 */
@Data
public class EventGroupLuckyKing {
    @RequireValue(EventMessageTypeConstans.NOTICE)
    private String post_type;
    private String notice_type;
    @RequireValue(EventMessageTypeConstans.LUCKY_KING)
    private String sub_type;
    private int group_id	;
    private   long   user_id;
    private  long target_id;

}
