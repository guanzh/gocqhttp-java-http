package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 * 群管理员变动
 * 事件数据
 *
 * 字段名	数据类型	可能的值	说明
 * time	int64	-	事件发生的时间戳
 * self_id	int64	-	收到事件的机器人 QQ 号
 * post_type	string	notice	上报类型
 * notice_type	string	group_admin	通知类型
 * sub_type	string	set、unset	事件子类型, 分别表示设置和取消管理员
 * group_id	int64	-	群号
 * user_id	int64	-	管理员 QQ 号
 */
@Data
public class EventGroupAdmin {
    private  long time;
    private int self_id	;
    @RequireValue(EventMessageTypeConstans.NOTICE)
    private String post_type;

    @RequireValue(EventMessageTypeConstans.GROUP_ADMIN)
    private String notice_type;
    private String sub_type;
    private int group_id;
    private   int   user_id;

}
