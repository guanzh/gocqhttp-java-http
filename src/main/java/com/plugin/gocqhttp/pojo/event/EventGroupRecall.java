package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 群禁言
 事件数据

 字段名	数据类型	可能的值	说明
 time	int64	-	事件发生的时间戳
 self_id	int64	-	收到事件的机器人 QQ 号
 post_type	string	notice	上报类型
 notice_type	string	group_ban	通知类型
 sub_type	string	ban、lift_ban	事件子类型, 分别表示禁言、解除禁言
 group_id	int64	-	群号
 operator_id	int64	-	操作者 QQ 号
 user_id	int64	-	被禁言 QQ 号
 duration	int64	-	禁言时长, 单位秒
 */
@Data
public class EventGroupRecall {

    private  long time;
    private int self_id	;
    @RequireValue(EventMessageTypeConstans.NOTICE)
    private String post_type;
    @RequireValue(EventMessageTypeConstans.GROUP_RECALL)
    private String notice_type;
    private long group_id;
    private  long operator_id;
    private   long   user_id;
    private  long message_id;

}
