package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 群成员增加
 事件数据

 字段名	数据类型	可能的值	说明
 time	int64	-	事件发生的时间戳
 self_id	int64	-	收到事件的机器人 QQ 号
 post_type	string	notice	上报类型
 notice_type	string	group_increase	通知类型
 sub_type	string	approve、invite	事件子类型, 分别表示管理员已同意入群、管理员邀请入群
 group_id	int64	-	群号
 operator_id	int64	-	操作者 QQ 号
 user_id	int64	-	加入者 QQ 号
 #群禁言
 */
@Data
public class EventGroupIncreaseAndDecrease {

    private  long time;
    private int self_id	;

    @RequireValue(value = EventMessageTypeConstans.NOTICE)
    private String post_type;
    @RequireValue(value = {EventMessageTypeConstans.GROUP_DECREASE,EventMessageTypeConstans.GROUP_INCREASE},logic = 1)
    private String notice_type;
    private String sub_type;
    private int group_id;
    private  int operator_id;
    private   int   user_id;

}
