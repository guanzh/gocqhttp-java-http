package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

@Data
public class EventGroup {
    private String raw_message;
    private int self_id;
    private int message_id;

    @RequireValue(EventMessageTypeConstans.GROUP)
    private String message_type;

    private String message;
    private int group_id;
    private SenderDTO sender;
    private String sub_type;
    private int user_id;
    private Object anonymous;
    @RequireValue(EventMessageTypeConstans.MESSAGE)
    private String post_type;
    private int time;
    private int message_seq;
    private int font;

    public static class SenderDTO {
        private String area;
        private String role;
        private String level;
        private String sex;
        private String title;
        private int user_id;
        private String nickname;
        private int age;
        private String card;
    }
}
