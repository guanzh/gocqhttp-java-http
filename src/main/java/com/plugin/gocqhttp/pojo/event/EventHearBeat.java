package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

@Data
public class EventHearBeat {


    private int self_id;
    private int interval;
    private String post_type;
    private int time;
    @RequireValue(EventMessageTypeConstans.HEAR_BEAT)
    private String meta_event_type;
    private StatusDTO status;

    public static class StatusDTO {
        private Object plugins_good;
        private StatDTO stat;
        private boolean good;
        private boolean app_good;
        private boolean app_initialized;
        private boolean online;
        private boolean app_enabled;

        public static class StatDTO {
            private int messageSent;
            private int lostTimes;
            private int packetReceived;
            private int messageReceived;
            private int packetSent;
            private int lastMessageTime;
            private int disconnectTimes;
            private int packetLost;
        }
    }
}
