package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

@Data
public class EventPrivate {

    private String raw_message;
    private int self_id;
    private int message_id;

    @RequireValue(EventMessageTypeConstans.PRIVATE)
    private String message_type;

    private int target_id;

    @RequireValue(EventMessageTypeConstans.MESSAGE)
    private String message;
    private SenderDTO sender;
    private String sub_type;
    private int user_id;
    private String post_type;
    private int time;
    private int font;

    public static class SenderDTO {
        private String sex;
        private int user_id;
        private String nickname;
        private int age;
    }
}
