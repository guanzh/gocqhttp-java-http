package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 * 好友添加对象
 */
@Data
public class EventFriendAdd {
    private int time;
    private int self_id;
    @RequireValue(EventMessageTypeConstans.NOTICE)
    private String post_type;

    @RequireValue(EventMessageTypeConstans.FRIEND_ADD)
    private String request_type;

    private int user_id;
    private String comment;
    private String flag;
}
