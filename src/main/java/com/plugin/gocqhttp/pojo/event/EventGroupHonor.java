package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 * 群成员荣誉变更提示
注意

此事件无法在手表协议上触发

事件数据

字段	类型	可能的值	说明
post_type	string	notice	上报类型
notice_type	string	notify	消息类型
group_id	int64		群号
sub_type	string	honor	提示类型
user_id	int64		成员id
honor_type	string	talkative:龙王 performer:群聊之火 emotion:快乐源泉	荣誉类型
 */
@Data
public class EventGroupHonor {
    @RequireValue(EventMessageTypeConstans.NOTICE)
    private String post_type;
    private String notice_type;
    private int group_id;
    @RequireValue(EventMessageTypeConstans.HONOR)
    private String sub_type;
    private int user_id;
    private String honor_type;
}
