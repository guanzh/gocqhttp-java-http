package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 * 群成员名片更新
注意

此事件不保证时效性, 仅在收到消息时校验卡片

事件数据

字段	类型	可能的值	说明
post_type	string	notice	上报类型
notice_type	string	group_card	消息类型
group_id	int64		群号
user_id	int64		成员id
card_new	string		新名片
card_old	string		旧名片
 */
@Data
public class EvenGroupCard {
    @RequireValue(EventMessageTypeConstans.NOTICE)
    private String post_type;
    @RequireValue(EventMessageTypeConstans.GROUP_CARD)
    private String notice_type;
    private int group_id;
    private int user_id;
    private String card_new;
    private String card_old;
}
