package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 * 接收到离线文件
事件数据

字段	类型	可能的值	说明
post_type	string	notice	上报类型
notice_type	string	offline_file	消息类型
user_id	int64		发送者id
file	object		文件数据
 */
@Data
public class EvenOfflineFile {
    private String post_type;
    @RequireValue(EventMessageTypeConstans.OFFLINE_FILE)
    private String notice_type;
    private int user_id;
    private Object file;
   
}
