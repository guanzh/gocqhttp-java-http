package com.plugin.gocqhttp.pojo.event;

import com.plugin.gocqhttp.pojo.EventMessageTypeConstans;
import com.plugin.gocqhttp.pojo.annotation.RequireValue;
import lombok.Data;

/**
 * 群添加对象
 */
@Data
public class EventGroupAdd {
    private int time;
    private int self_id;
    @RequireValue(EventMessageTypeConstans.REQUEST)
    private String post_type;
    @RequireValue(EventMessageTypeConstans.FRIEND)
    private String request_type;

    @RequireValue(value = {EventMessageTypeConstans.SUB_TYPE_ADD,EventMessageTypeConstans.SUB_TYPE_INVITE},logic = 1)
    private String sub_type;
    private int group_id;
    private int user_id;
    private String comment;
    private String flag;
}
