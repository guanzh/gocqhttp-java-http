package com.plugin.gocqhttp.pojo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 请求判断必要的值
 * @author 1
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface RequireValue {
    String[] value() default "";

    // 多个参数 0表示and  1表示or
    int logic() default 0;
}
