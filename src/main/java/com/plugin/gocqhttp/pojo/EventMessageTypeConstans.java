package com.plugin.gocqhttp.pojo;

public class EventMessageTypeConstans {

    public static final String GROUP="group";
    public static final String GROUP_UPLOAD="group_upload";
    public static final String GROUP_ADMIN="group_admin";
    public static final String GROUP_BAN="group_ban";
    public static final String GROUP_RECALL="group_recall";
    public static final String GROUP_CARD="group_card";
    public static final String GROUP_INCREASE="group_increase";
    public static final String GROUP_DECREASE="group_decrease";

    public static final String MESSAGE="message";

    public static final String NOTICE="notice";

    public static final String REQUEST="request";

    public static final String PRIVATE="private";

    public static final String HONOR="honor";

    public static final String OFFLINE_FILE="offline_file";


    public static final String FRIEND="friend";
    public static final String FRIEND_ADD="friend_add";
    public static final String FRIEND_RECALL="friend_recall";

    public static final String POKE="poke";

    public static final String LUCKY_KING="lucky_king";

    public static final String CLIENT_STATUS="client_status";

    public static final String ESSENCE="essence";

    public static final String SUB_TYPE_ADD="add";

    public static final String SUB_TYPE_INVITE="invite";

    public static final String HEAR_BEAT="heartbeat";

}
