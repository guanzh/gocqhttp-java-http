package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 获取精华消息列表
终结点: /get_essence_msg_list

参数

字段	类型	说明
group_id	int64	群号
*/
@Data
public class GetEssenceMsgList{
    private int group_id;
    
}