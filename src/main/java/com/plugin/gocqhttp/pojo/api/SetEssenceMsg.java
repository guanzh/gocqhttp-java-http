package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 设置精华消息
终结点: /set_essence_msg

参数

字段	类型	说明
message_id	int32	消息ID
 */
@Data
public class SetEssenceMsg{
    private int message_id;
    
}