package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 设置在线机型
提示

有关例子可从这个链接找到

终结点：/_set_model_show

参数

字段	类型	说明
model	string	机型名称
model_show	string	-
 */
@Data
public class SetModelShow{
    private String model;
    private String  model_show;
    
}