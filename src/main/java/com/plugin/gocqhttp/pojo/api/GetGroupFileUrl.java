package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 获取群文件资源链接
提示

File 和 Folder 对象信息请参考最下方

终结点: /get_group_file_url

参数

字段	类型	说明
group_id	int64	群号
file_id	string	文件ID 参考 File 对象
busid	int32	文件类型 参考 File 对象
 */
@Data
public class GetGroupFileUrl{
    private int group_id;
    private String file_id;
    private int busid;
}