package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 下载文件到缓存目录
终结点: /download_file

参数

字段	类型	说明
url	string	链接地址
thread_count	int32	下载线程数
headers	string or array	自定义请求头
 */
@Data
public class DownloadFile{
    private String url;
    private int thread_count;
    private String headers;
}