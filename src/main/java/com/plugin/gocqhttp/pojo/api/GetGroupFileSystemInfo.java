package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 获取群文件系统信息
终结点: /get_group_file_system_info

参数

字段	类型	说明
group_id	int64	群号
 */
@Data
public class GetGroupFileSystemInfo{
    private int group_id;
    
}