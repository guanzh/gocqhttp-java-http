package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * group_id	int64	群号
 * file	string	图片文件名
 * cache	int	表示是否使用已缓存的文件
 */
@Data
public class CleanCache{
    private int group_id	;//int64	群号
    private String file;//	string	图片文件名
    private int cache;//	int	表示是否使用已缓存的文件
}
