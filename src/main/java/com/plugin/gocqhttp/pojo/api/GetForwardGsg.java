package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 获取合并转发内容
终结点: /get_forward_msg

参数

字段	类型	说明
message_id	string	消息id
 */
@Data
public class GetForwardGsg{
    public String message_id;
    
}