package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 撤回消息
终结点：/delete_msg

参数

字段名	数据类型	默认值	说明
message_id	int32	-	消息 ID
 */
@Data
public class DeleteMsg{
    private int message_id;
    
}