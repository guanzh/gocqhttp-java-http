package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 检查链接安全性
终结点：/check_url_safely

参数

字段	类型	说明
url	string	需要检查的链接
 */
@Data
public class CheckUrlSafely{
    private String url;
    
}