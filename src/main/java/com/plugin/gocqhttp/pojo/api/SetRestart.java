package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * delay	number	0	要延迟的毫秒数, 如果默认情况下无法重启, 可以尝试设置延迟为 2000 左右
 */
@Data
public class SetRestart {
    private long number;
}
