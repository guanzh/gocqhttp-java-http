package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 发送群公告
终结点： /_send_group_notice

参数

字段名	数据类型	默认值	说明
group_id	int64		群号
content	string		公告内容
image	string		图片路径（可选）
 */
@Data
public class SendGroupNotice{
    private int group_id;
    private String content;
    private String image;
}