package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 删除好友
终结点：/delete_friend

参数

字段名	数据类型	默认值	说明
friend_id	int64	-	好友 QQ 号
 */
@Data
public class DeleteFriend{
    private int friend_id;
    
}