package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 添加组
 */
@Data
public class ApiGroupAdd {
    private String flag;
    private String sub_type;
    private String type;
    private boolean approve;
    private String reason;// 拒绝理由（仅在拒绝时有效）
}
