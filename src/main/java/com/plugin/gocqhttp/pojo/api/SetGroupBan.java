package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 群组单人禁言
终结点：/set_group_ban

参数

字段名	数据类型	默认值	说明
group_id	int64	-	群号
user_id	int64	-	要禁言的 QQ 号
duration	number	30 * 60	禁言时长, 单位秒, 0 表示取消禁言
 */
@Data
public class SetGroupBan{
    private int group_id;
    private int user_id;
    private int duration;
}