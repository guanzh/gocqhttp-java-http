package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 上传群文件
终结点: /upload_group_file

参数

字段	类型	说明
group_id	int64	群号
file	string	本地文件路径
name	string	储存名称
folder	string	父目录ID
 */
@Data
public class UploadGroupFile{
    private int group_id;
    private String file;
    private String name;
    private String folder;
}