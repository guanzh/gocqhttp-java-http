package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * app_initialized	bool	原 CQHTTP 字段, 恒定为 true
 * app_enabled	bool	原 CQHTTP 字段, 恒定为 true
 * plugins_good	bool	原 CQHTTP 字段, 恒定为 true
 * app_good	bool	原 CQHTTP 字段, 恒定为 true
 * online	bool	表示BOT是否在线
 * good	bool	同 online
 * stat	Statistics	运行统计
 *       PacketReceived	uint64	收到的数据包总数
 *         PacketSent	uint64	发送的数据包总数
 *         PacketLost	uint32	数据包丢失总数
 *         MessageReceived	uint64	接受信息总数
 *         MessageSent	uint64	发送信息总数
 *         DisconnectTimes	uint32	TCP 链接断开次数
 *         LostTimes	uint32	账号掉线次数
 *         LastMessageTime	int64	最后一条消息时间
 */
@Data
public class GetSatas{

    private boolean app_initialized;
    private boolean app_enabled;
    private boolean plugins_good;
    private boolean app_good;
    private boolean online;
    private boolean good;
    private boolean stat;
    @Data
    class Statisticsextends  {
       private int PacketReceived;
       private int PacketSent;
       private int PacketLost;
       private int MessageReceived;
       private int MessageSent;
       private int DisconnectTimes;
       private int LostTimes;
       private int LastMessageTime;
    }

}
