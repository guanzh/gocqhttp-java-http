package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 字段	类型	说明
 * content	string	内容
 */
@Data
public class GetWordSlices{
    private String content;

}
