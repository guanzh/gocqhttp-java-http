package com.plugin.gocqhttp.pojo.api;
import lombok.Data;
/**
 * 设置群名
终结点：/set_group_name

参数

字段名	数据类型	说明
group_id	int64	群号
group_name	string	新群名
 */
@Data
public class SetGroupName{
    private int group_id;
    private String group_name;
    
}
