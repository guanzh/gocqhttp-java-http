package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 *获取在线机型
提示

有关例子可从这个链接找到

终结点：/_get_model_show

参数

字段	类型	说明
model	string	机型名称
 */
@Data
public class GetModelShow{
    private String model;
    
}