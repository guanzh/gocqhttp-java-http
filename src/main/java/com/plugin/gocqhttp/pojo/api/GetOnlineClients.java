package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 获取当前账号在线客户端列表
终结点：/get_online_clients

参数

字段	类型	说明
no_cache	bool	是否无视缓存
 */
@Data
public class GetOnlineClients{
    private boolean no_cache;

}