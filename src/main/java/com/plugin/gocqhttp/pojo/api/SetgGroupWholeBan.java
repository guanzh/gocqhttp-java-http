package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 群组全员禁言
终结点：/set_group_whole_ban

参数

字段名	数据类型	默认值	说明
group_id	int64	-	群号
enable	boolean	true	是否禁言
 */
@Data
public class SetgGroupWholeBan{
    private int group_id;
    private boolean enable;
}