package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 重载事件过滤器
终结点：/reload_event_filter

参数

字段名	数据类型	默认值	说明
file	string	-	事件过滤器文件
 */
@Data
public class ReloadEventFilter{
    private String file;
    
}