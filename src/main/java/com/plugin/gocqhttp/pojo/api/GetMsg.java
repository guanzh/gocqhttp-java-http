package com.plugin.gocqhttp.pojo.api;

import lombok.Data;

/**
 * 获取消息
终结点: /get_msg

参数

字段	类型	说明
message_id	int32	消息id
 */
@Data
public class GetMsg{
    public int message_id;
    
}