package com.plugin.gocqhttp.pojo;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.util.List;

@Data
public class ResponseList {
    private ResponseInfo responseInfo;

    public static ResponseList getBuilder(){
        return new ResponseList() {};
    }
    public ResponseList setData(ResponseInfo responseInfo){
        this.responseInfo = responseInfo;
        return  this;
    }

    public List  build(Class clazz) {
        return JSON.parseArray(JSON.toJSONString(responseInfo.getData()),clazz);
    }

}
