package com.plugin.gocqhttp.pojo;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;

@Data
public class ResponseInfo {
    private JSONArray data;
    private int retcode;
    private String status;
}
