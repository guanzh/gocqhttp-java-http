package com.plugin.gocqhttp.configuration;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Accessors(chain = true)
@Configuration
@ConfigurationProperties(prefix = "gocqhttp")
public class GoCqhttpPropertis {
    private String addr;
    /**
     * 心跳超时
     */
    private  long heartimeout=50000;
}
