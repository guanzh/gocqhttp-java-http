package com.plugin.gocqhttp.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.plugin.gocqhttp"})
public class EventHandlerConfiguration {
    @Bean
    public EventHandlerRegistBeanFactory eventHandlerRegistBeanFactory(){
        return new EventHandlerRegistBeanFactory();
    }

}
